#!/usr/bin/env python

from jira import JIRA
import dateutil.parser
import argparse
import getpass
import sys

parser = argparse.ArgumentParser()
parser.add_argument("-s", "--server", help="The JIRA instance to connect to, including context path.")
parser.add_argument("-u", "--username", help="The username to connect to this JIRA instance with.")
parser.add_argument("-p", "--password", help="The password associated with this user.")
parser.add_argument("-P", "--prompt", action="store_true", help="Prompt for the password at the command line.")
parser.add_argument("-q", "--jql", help="JQL")
args = parser.parse_args()

def usage():
    print("Usage: %s [-s SERVER] -u USERNAME [-p PASSWORD] [-P] -q 'JQL'" % sys.argv[0])

if args.prompt:
    jpasswd = getpass.getpass("Password: ")
elif args.password:
    jpasswd = args.password
else:
    usage()
    sys.exit(1)

if args.server:
    jserver = args.server
else:
    jserver = 'https://jcrewtracker.jira.com/'
    print("Using default server, %s . . ." % jserver)

if args.username:
    pass
else:
    usage()
    sys.exit(1)

if args.jql:
    jql = args.jql
else:
    jql="project=JCD AND issuetype='Global Nav Update' AND status='Closed'"

jira = JIRA(server=jserver ,basic_auth=(args.username,jpasswd))
#issues = jira.search_issues("project=MWD AND issuetype='Global Nav Update' AND status='Closed'",
issue_counter = 0
issue_increment = 50
issues = jira.search_issues(jql, startAt=issue_counter, expand='changelog')
times = []
while issue_counter < issues.total:
    for i in issues:
        start_time = None
        end_time = None
        print("Issue: ", i.key, "\n")
        for hists in i.changelog.histories:
            #print("\tHistory Created: ", hists.created, "\n")
            for item in hists.items:
                if item.field == 'status' and item.toString == 'Pending Preview Deployment':
                    #print("\t\t", item.field, item.fromString, item.toString, "\n")
                    start_time = hists.created
                elif item.field == 'status' and item.toString == 'Pending QA Deployment':
                    #print("\t\t", item.field, item.fromString, item.toString, "\n")
                    end_time = hists.created
                if start_time and end_time:
                    diff = dateutil.parser.parse(end_time) - dateutil.parser.parse(start_time)
                    print("Diff: ", diff)
                    if diff.days > 0 or diff.seconds < 15*60 or diff.seconds > 60*60*8:
                        print("NOT adding %s" % i.key)
                    else:
                        times.append(diff)
                    break
            if start_time and end_time:
                #print("for %s, start = %s, end = %s" % (i.key, start_time, end_time))
                break
    issue_counter += issue_increment
    issues = jira.search_issues(jql, startAt=issue_counter, expand='changelog')

print(times)
seconds = [ x.seconds for x in times ]
print(seconds)
print("There were %i tickets, with avg = %s mins" % (len(seconds), sum(seconds)/len(seconds)/60))
